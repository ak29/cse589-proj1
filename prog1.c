#include <stdio.h>
#include <string.h>
#include <stdbool.h> // for bool datatype
#include <ctype.h>
#include <unistd.h> // for host info - display() function
#include <stdlib.h> //exit(0);
#include <arpa/inet.h>
#include <sys/socket.h>
#include <net/if.h>
#include <ifaddrs.h>
#include <error.h>


bool isNumber(char number[]){
	int i;
	for(i=0 ;number[i] != 0; i++){
		if(!isdigit(number[i]))
			return false;

	}
	return true;
}

void help(){
	printf(" \n\n ======================================== HELP SECTION =============================================== \n\n");
	printf(" HELP - Displays the help for all commands for the application \n\n");
	printf(" CREATOR - Displays the author information \n\n");
	printf(" DISPLAY - Displays the IP Address and Port Information of current process \n\n");
	printf(" REGISTER <serverIP> <port no> - ** CLIENT ONLY ** - Allows clients to register with the server. \n\n");
	printf(" CONNECT <destination> <port no> - Allows registered clients to connect to a different file sharing host. \n\n");
	printf(" LIST - Lists all the connections made from and to the current program. \n\n");
	printf(" TERMINATE <connection ID> - Allows to terminate a connection displayed by the LIST command. \n\n");
	printf(" QUIT - Allows a client to kill connections gracefully and quit the network. \n\n");
	printf(" GET <connection ID> <file> - ** CLIENT ONLY ** - This command will download a file from one host specified in the command. \n\n");
	printf(" PUT <connection ID> <filename> - ** CLIENT ONLY ** - This command will download a file from one host specified in the command \n\n");		
	printf(" \n\n ======================================== END OF HELP SECTION =============================================== \n\n");
}

void creator(){
	printf(" \n\n ======================================== CREDITS =============================================== \n\n");	
	printf(" AUTHOR NAME      : ABHISHEK ARAVIND KULKARNI \n");
	printf(" UBIT NAME        : ak229\n");
	printf(" UB EMAIL ADDRESS : ak229@buffalo.edu\n");
	printf(" \n\n ======================================== END OF CREDITS =============================================== \n\n");
}

void die(char *s)
{
    perror(s);
    exit(1);
}
 

void display(int hostID, char* portnumber){

	struct ifaddrs *interfaceAddr;
	struct sockaddr_in* ipv4_addr;
	char* ipaddr;

	getifaddrs(&interfaceAddr); // Gets the information for all interfaces.

	while(interfaceAddr)
	{
		ipv4_addr = (struct sockaddr_in *)interfaceAddr->ifa_addr; // stores the IP address field from the interface
		if(ipv4_addr->sin_family == AF_INET) 
		{
			ipaddr = (char*) malloc(sizeof(char)*INET_ADDRSTRLEN);  // creates destination buffer to store the addresses
			inet_ntop(AF_INET, &(ipv4_addr->sin_addr),ipaddr,INET_ADDRSTRLEN);
		}

		interfaceAddr = interfaceAddr->ifa_next; // Loop till the last interface.
	}
	// Iterates over all interfaces starting with loopback, and iterating all the way to the final interface, that is external facing
	printf(" \n\n ======================================== DISPLAY =============================================== \n\n");	

	if(hostID == 0){
		printf("The program is running as Server on IP %s : %s\n",ipaddr,portnumber);
	}
	else{
		printf("The program is running as Client on IP %s : %s\n",ipaddr,portnumber);	
	}
	printf(" \n\n ======================================== END OF DISPLAY =============================================== \n\n");

}




void register_client(){
	printf(" REGISTERING YOU AS A CLIENT ");
}


int main(int argc, char *argv[]){

	// Ensuring right number of arguments
	if(argc != 3){
		printf("Invalid number of arguments. \n Run the program as ./prog1 <s/c> <portnumber> \n");
		return -1;
	}

	// Ensuring the arguments are right
	// Ensuring first argument is right	
	const char* type_server = "s";
	const char* type_client = "c";

	int hostIdentifier = -1;
	if(strcmp(type_server,argv[1]) == 0){
		printf(" Started the program as a server \n");
		hostIdentifier = 0;
	}
	else if(strcmp(type_client,argv[1]) == 0){
		printf(" Started the program as a client \n");
		hostIdentifier = 1;
	}
	else{
		printf(" Invalid 1st argument.\n Has to be either s for server and c for client \n");
		return -1; 
	}

	//Ensuring second argument is right
	if(!isNumber(argv[2])){
		printf(" Nope, 2nd argument needs to be a valid port number\n ");
		return -1;
	}

	//Setting up the menu 
	char CMD_INPUT[80];

	int input_number;
	while(true){
		printf(" Choose one of the following options to be run. \n");
		printf(" %d. HELP \n",1);
		printf(" %d. CREATOR \n",2);
		printf(" %d. DISPLAY \n",3);
		printf(" %d. REGISTER <serverIP> <port no> \n",4);
		printf(" %d. CONNECT <destination> <port no> \n",5);
		printf(" %d. LIST \n",6);
		printf(" %d. TERMINATE <connectionID> \n",7);
		printf(" %d. QUIT \n",8);
		printf(" %d. GET <connectionID> <file> \n",9);
		printf(" %d. PUT <connectionID> <file> \n",10);

		scanf("%s", CMD_INPUT);
		
		switch(CMD_INPUT){
			case 1:
			{
				help();
				break;
			}
			case 2:
			{
				creator();
				break;
			}
			case 3:{
				display(hostIdentifier, argv[2]);
				break;
			}
			case 4:{
				if(hostIdentifier != 1){
					printf("Can not run register as a server.");
				}
				else{
					register_client();
				}
				break;
			}
			case 8:{
				printf(" Quitting the program as client/host \n");
				return 0;
			}
			default:
			{
				printf("Enter a valid input from 1 - 10 \n");
				break;
				return -1;
			}
		}	
	}

	return 0;
}
